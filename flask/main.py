from flask import Flask, Response, request
from prometheus_client import generate_latest, Counter, Gauge, Histogram, Summary, Info, REGISTRY
from time import time

app = Flask(__name__)

# Métricas:

## Counter: Quantos doces foram vendidos
doces_vendidos = Counter('doces_vendidos', 'Número de doces vendidos')

## Gauge: Quantos doces ainda estão na prateleira
doces_na_prateleira = Gauge('doces_na_prateleira', 'Número de doces na prateleira')

## Histogram: Tempo que as crianças gastam na loja
tempo_na_loja = Histogram('tempo_na_loja', 'Tempo que as crianças passam na loja')

## Info: Informações sobre a loja
info_loja = Info('info_loja', 'Informações sobre a loja de doces')

# Variáveis:
criancas_na_loja = {}

@app.route('/')
def home():
    return "Bem vindos a loja de doces !"

@app.route('/reabastecer/<int:quantidade>')
def reabastecer(quantidade):
    doces_na_prateleira.inc(quantidade)
    return f"{quantidade} doces adicionados à prateleira!"

@app.route('/vender_doce/<int:quantidade>')
def vender_doce(quantidade):
    doces_vendidos.inc(quantidade)
    doces_na_prateleira.dec(quantidade)
    return f"{quantidade} doces vendidos!"

@app.route('/entrar_loja/<string:crianca_id>')
def entrar_loja(crianca_id):
    criancas_na_loja[crianca_id] = time()
    return f"A criança {crianca_id} entrou na loja!"

@app.route('/sair_loja/<string:crianca_id>')
def sair_loja(crianca_id):
    if crianca_id not in criancas_na_loja:
        return f"A criança {crianca_id} não está na loja!", 400
    
    tempo_entrada = criancas_na_loja[crianca_id]
    tempo_gasto_na_loja = time() - tempo_entrada
    tempo_na_loja.observe(tempo_gasto_na_loja / 60)

    del criancas_na_loja[crianca_id]

    return f"{crianca_id} passou {tempo_gasto_na_loja / 60:.2f} minutos na loja e agora saiu."

@app.route('/mudar_nome_loja/<string:nome_loja>')
def mudar_nome_loja(nome_loja):
    info_loja.info({'nome': nome_loja})
    return f"O nome da loja mudou para {nome_loja} !"


@app.route('/metrics')
def metrics():
    return Response(generate_latest(REGISTRY), mimetype='text/plain')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
