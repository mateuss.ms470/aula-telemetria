# Instruções para Execução do Projeto

## Pré-requisitos: Docker Compose

### Instalação do Docker Compose:

1. **Baixe o Docker Compose**:
   ```bash
   sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   ```

2. **Dê permissões de execução ao binário**:
   ```bash
   sudo chmod +x /usr/local/bin/docker-compose
   ```

3. **Verifique se o Docker Compose foi instalado corretamente**:
   ```bash
   docker-compose --version
   ```
   Isso deve retornar a versão que você instalou, algo como:
   ```
   docker-compose version 1.29.2
   ```

## Executando o Projeto:

1. **Instale as dependências**:
   ```bash
   docker-compose build
   ```

2. **Inicie os serviços**:
   ```bash
   docker-compose up
   ```

## Acessando as Aplicações:

### Flask:

- **URL**: `localhost:5000`

- **Rotas**:
  - `/`
  - `/reabastecer/<int:quantidade>`
  - `/vender_doce/<int:quantidade>`
  - `/entrar_loja/<string:crianca_id>`
  - `/sair_loja/<string:crianca_id>`
  - `/mudar_nome_loja/<string:nome_loja>`
  - `/metrics`

### Prometheus:

- **URL**: `localhost:9090`
  
  - Em **Status > Targets**, você pode ver as aplicações que estão sendo monitoradas.
  - Na tela principal, você pode analisar as métricas.

### Grafana:

- **URL**: `localhost:3000`

  - Você pode monitar as métricas, após iniciar a aplicação no [Dashboard de monitoramento da aplicação](http://localhost:3000/d/a38eb82c-c4dc-42ca-b295-9afa107d6d8e/monitoramento-estoque?orgId=1&refresh=5s)